import sys
import operator
import graph_fun
import pdb
from collections import deque
column_number=0
row_number=0
count=0


# helper function to generate img needed for animation
def print_graph(graph,state,start_state):
  global count
  ly=[]
  ls = get_list(graph,state,start_state,ly)
  pdb.set_trace()
  length=len(ls)
  for entry in reversed(ls):
    count+=1
    update_g(graph)
    graph[entry[1]][entry[0]].update_value('P')
    graph[entry[3]][entry[2]].update_value('G')
    filename="E:/cs440/MP1/output/"+str(count)+".png"
    graph_fun.generate_img(graph,filename)
    
# put the path nodes in to a list for print
def get_list(graph,state,start_state, ls):
  #pdb.set_trace()
  if state.parent:
    if state.parent == start_state:
      pdb.set_trace()
      return ls
    else:
      ls.append([state.parent.px,state.parent.py,state.parent.gx,state.parent.gy])
      return get_list(graph,state.parent,start_state,ls)

# helper function to erase previous P and G in graph
def update_g(graph):
  for i in range(0, len(graph)):
    for j in range(0, len(graph[0])):
      if graph[i][j].v == 'g' or graph[i][j].v == 'G' or graph[i][j].v == "P":
        graph[i][j].v = " "

# helper function to check whether one state is in a specific list        
def in_list(input,ls):
  for state in ls:
    if (input.px == state.px and input.py == state.py and input.gx == state.gx and input.gy == state.gy and input.gd == state.gd):
      return True

  return False


# helper function to check whether the pecman
# will collide with ghost
def will_collide_ghost(graph,state,node,ghost,isGoingRight):
  ghostNext = ghost
  if((ghostNext.r == state.py and ghostNext.c == state.px )
    or (isGoingRight[0] and state.px == ghost.c and state.px == node.c-1)
    or ((not isGoingRight[0]) and state.px == ghost.c and state.px == node.c+1)): # test if pacman will meet the ghost or cross it
      return True
  return False

# the main a* search function which take in 
# the graph, a visited list, a wait list and
# a step counter
def a_star(graph, visited, wait_list, steps):
  ghost = graph_fun.get_ghost_start(graph)
  # the init direction of ghost is right(True)
  isGoingRight = [True]
  start_state = wait_list[0]
  while True:
    try:
      state = wait_list.pop(0)
      # get the ghost node in current state
      ghost = graph[state.gy][state.gx]
      # get the ghost direction in current state
      isGoingRight[0] = state.gd
      # get the pecman node in current state
      node = graph[state.py][state.px]
      visited.append(state)
      if node.v == '.':
        print_graph(graph,state,start_state)
        steps[0] = state.g
        return True
      elif node.v == '%':
        continue
      else:
        ghost = graph_fun.get_ghost_next(graph,ghost,isGoingRight) # update ghost to next position
        # get the possible next states
        neighbor=graph_fun.get_state_neighbors(graph,state,ghost,isGoingRight)
        for one in neighbor:
          if (not in_list(one,visited) and not will_collide_ghost(graph,one,node,ghost,isGoingRight)):
            if (not in_list(one,wait_list)):
              # if the node hasn't be expanded yet
              # simply add it to the wait list
              one.save_parent(state)
              one.update_g(state.g+1)
              one.update_f(one.g + graph[one.py][one.px].h)
              wait_list.append(one)

            elif one.g<state.g+1:
              # if the node already in the wait list
              # update the cost to the node to the lower one
                one.save_parent(state)
                one.update_g(state.g+1)
                one.update_f(one.g + graph[one.py][one.px].h)

        wait_list.sort(key=operator.attrgetter('f'))
    except IndexError:
      print ("Sorry! Cannot find a path to the end!")
      print (" x = " + str(state.px) + " y = " + str(state.py))
      return False


# Start Program
while True:
  maze = input("Please choose the maze you want to play:\n"
               "1. Small Maze\n"
               "2. Medium Maze\n"
               "3. Large Maze\n"
               "4. Exit\n"
               "Your choice:")
  if maze == 4:
    sys.exit()
  elif maze == 1:
    f = open('ghostmaze/smallGhost.txt', 'r')
  elif maze == 2:
    f = open('ghostmaze/mediumGhost.txt', 'r')
  elif maze == 3:
    f = open('ghostmaze/bigGhost.txt', 'r')
  else:
    print ("Invalid input of maze!")
    sys.exit()

  graph=graph_fun.construct_graph(f)
  graph_fun.print_graph(graph)
  for i in range(0, len(graph)):
    for j in range(0, len(graph[0])):
      # find the start and end point
      if graph[i][j].v == 'P':
        start_row = graph[i][j].r
        start_column = graph[i][j].c
      if graph[i][j].v == '.':
        end_row = graph[i][j].r
        end_column = graph[i][j].c
  for i in range(0, len(graph)):
    for j in range(0, len(graph[0])):
      graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column))

# initialize variables and start search
  visited = []
  # find the start ghost
  ghost = graph_fun.get_ghost_start(graph)
  new_state = [graph_fun.State(start_column,start_row,ghost.c,ghost.r,True)]
  wait_list = [graph[start_row][start_column]]
  steps = [0]
  a_star(graph, visited, new_state,steps)
# print solved path and related data
  graph_fun.print_graph(graph)
  print ("Total node:"+str(len(visited)))
  print ("Total steps:" + str(steps[0]))


