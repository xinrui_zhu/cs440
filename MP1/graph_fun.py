import sys
import operator
from collections import deque
from PIL import Image, ImageDraw, ImageFont
column_number=0
row_number=0

# the class represent every location
class Node:

  # only r,c,v,h used in 1.3
  parent = None
  g = 0
  h = 0
  f = 0
  def __init__(self, row, column, value):
    # only this three value will be used in the 1-3
    self.r = row
    self.c = column
    self.v = value
  def update_value(self, value):
    self.v = value
  def save_parent(self, node):
    self.parent = node
  def add_heuristic(self, h):
    self.h = h
  def update_g(self, g):
    self.g = g
  def update_f(self):
    self.f = self.g+self.h

# the class represent searching state in 1-3
# contains the location of pecman and the ghost
# and the related information
class State:
  px=0
  py=0
  gx=0
  gy=0
  gd=True
  f=0
  g=0
  parent = None

  def __init__(self, x, y, ghostx, ghosty, ghostd):
    self.px = x
    self.py = y
    self.gx = ghostx
    self.gy = ghosty
    self.gd = ghostd
  def update_f(self, f):
    self.f = f
  def update_g(self, g):
    self.g = g
  def save_parent(self, state):
    self.parent = state

# helper function to print the graph
# to the console
def print_graph(graph):
  r = len(graph)
  c = len(graph[0])
  for i in range(0,r):
    for j in range(0,c):
      sys.stdout.write(graph[i][j].v)
    print ("\n"),

# function to test the heuristic value
def print_heuristic(graph):
  r = len(graph)
  c = len(graph[0])
  line = ""
  for i in range(0,r):
    for j in range(0,c):
      line = line+str(graph[i][j].h)+","
    print line
    line=""

# helper function to generate the image needed for animation
def generate_img(graph,filename):
  im = Image.new("RGB", (500, 500), "#FFF")
  font = ImageFont.truetype("consola.ttf", 20)
  draw = ImageDraw.Draw(im)
  line = ""
  for i in range(0,row_number):
    for j in range(0,column_number):
      line=line+graph[i][j].v
    print line
    draw.text((0,20*i),line,"#000",font)
    line=""
  im.save(filename)

# helper function to transfer the txt file
# to a graph where each location is a node obj
def construct_graph(f):
  global row_number
  global column_number
  row_number=0
  graph = []

  for line in f:
    line=line.rstrip('\n')
    row=[]
    column_number = 0
    for letter in line:
      new_node = Node(row_number, column_number, letter)
      row.append(new_node)
      column_number+=1
    graph.append(row)
    row_number+=1
  return graph


# helper function to find neighbors
def get_neighbor(graph, node):
  neighbor = []
  node_row = node.r
  node_column = node.c
  if node_row<row_number-1:
    if(graph[node_row+1][node_column].v!='%'):
      neighbor.append(graph[node_row+1][node_column])
  if node_column<column_number-1:
    if(graph[node_row][node_column+1].v!='%'):
      neighbor.append(graph[node_row][node_column+1])
  if node_row>0:
    if(graph[node_row-1][node_column].v!='%'):
      neighbor.append(graph[node_row-1][node_column])
  if node_column>0:
    if(graph[node_row][node_column-1].v!='%'):
      neighbor.append(graph[node_row][node_column-1])
  return neighbor

# helper function to determine whether a node
# is a dead end(for 1-2)
def dead_end(graph, node):
  neighbors = get_neighbor(graph, node)
  if len(neighbors)==1:
    return True
  else:
    return False

# helper function to determine whether pecman
# has to turn at(for 1-2)
def must_turn(graph, node):
  neighbors = get_neighbor(graph, node)
  if len(neighbors)==2:
    one = neighbors[0]
    two = neighbors[1]
    if (one.c!=two.c)and(one.r!=two.r):
      return True
  return False

# helper function to get the ghost
# start point(for 1-3)
def get_ghost_start(graph):
  r = len(graph)
  c = len(graph[0])
  for i in range(0,r):
    for j in range(0,c):
      if(graph[i][j].v == "G"):
        return graph[i][j]

# helper function to get the next 
# location of ghost(for 1-3)
def get_ghost_next(graph, ghost, isGoingRight):
  if(isGoingRight[0]):
    if(graph[ghost.r][ghost.c+1].v != "%"):
      return graph[ghost.r][ghost.c+1]
    else:
      isGoingRight[0] = False
      return graph[ghost.r][ghost.c-1]
  else:
    if(graph[ghost.r][ghost.c-1].v != "%"):
      return graph[ghost.r][ghost.c-1]
    else:
      isGoingRight[0] = True
      return graph[ghost.r][ghost.c+1]

# helper function to get next possible state from the current state
def get_state_neighbors(graph,state,ghost,isGoingRight):
  neighbor = []
  node_row = state.py
  node_column = state.px
  if node_row<row_number:
    if(graph[node_row+1][node_column].v!='%'):
      neighbor.append(State(node_column,node_row+1,ghost.c,ghost.r,isGoingRight[0]))
  if node_column<column_number:
    if(graph[node_row][node_column+1].v!='%'):
      neighbor.append(State(node_column+1,node_row,ghost.c,ghost.r,isGoingRight[0]))
  if node_row>0:
    if(graph[node_row-1][node_column].v!='%'):
      neighbor.append(State(node_column,node_row-1,ghost.c,ghost.r,isGoingRight[0]))
  if node_column>0:
    if(graph[node_row][node_column-1].v!='%'):
      neighbor.append(State(node_column-1,node_row,ghost.c,ghost.r,isGoingRight[0]))
  return neighbor


