import sys
import operator
import graph_fun
from collections import deque
column_number=0
row_number=0
img_number=0
path_cost=0
exp_nodes=0

# Depth-first search which take in the graph 
# a visited list and current node
# return True if find the location, False otherwise
def dfs(graph, visited, node):
  global img_number
  global path_cost
  global exp_nodes
  # add the current node to visited list
  visited.append(node)
  if node.v=='%':
    return False
  # get the node to expand next
  neighbor = graph_fun.get_neighbor(graph, node)
  for one in neighbor:
    if one not in visited:
      # check if find the destination
      exp_nodes+=1
      if one.v=='.':
        return True
      else:
        if dfs(graph, visited, one):
          one.update_value('.')
          path_cost+=1
          return True
  return False

# helper function to print img
def get_list(node, ls):
  global path_cost
  if node.parent:
    if node.parent.v != 'P':
      path_cost+=1
      ls.append(node.parent)
      return get_list(node.parent, ls)
    else:
      return ls
  else:
    return ls

# helper function to print img
def print_path(node):
  global img_number
  ls=[]
  ls=get_list(node, ls)
  for one in reversed(ls):
    one.update_value('.')
    graph_fun.generate_img(graph,"E:/cs440/MP1/output/"+str(img_number)+".png")
    img_number+=1

# mark the path from the passed in node to the start node
def bfs_mark_path(node):
  global path_cost
  if node.parent:
    if node.parent.v != 'P':
      path_cost+=1
      node.parent.update_value('.')
      bfs_mark_path(node.parent)


# Breadth-first search function which take in the graph 
# a visited list and a waiting queue
# return True if find the location, False otherwise
def bfs(graph, visited, queue):
  global exp_nodes
  while True:
    try:
      node = queue.popleft()
      # add current node to the visited list
      visited.append(node)
      # find destination
      if node.v=='.':
        bfs_mark_path(node)
        return True
      # if find wall, just continue
      elif node.v=='%':
        continue
      else:
        neighbor = graph_fun.get_neighbor(graph, node)
        #add new nodes to the queue
        for one in neighbor:
          if one not in visited:
            if one not in queue:
              exp_nodes+=1
              one.save_parent(node)
              queue.append(one)
    except IndexError:
      print ("Sorry! Cannot find a path to the end!")
      return False


# Greedy Breadth-first search function  
# Take in the graph a visited list and current node
# return True if find the location, False otherwise
def greedy_bfs(graph, visited, node):
  global exp_nodes
  global path_cost
  if node.v == '.':
    return True
  elif node.v == '%':
    return False
  else:
    # add current node to the visited list
    visited.append(node)
    neighbor = graph_fun.get_neighbor(graph, node)
    exp_nodes+=len(neighbor)
    # sort the neighbor by heuristic and 
    # choose the one with lowest heuristic first
    neighbor.sort(key=operator.attrgetter('h'))
    for one in neighbor:
      if one not in visited:
        if greedy_bfs(graph, visited, one):
          path_cost+=1
          if node.v!="P":
            node.update_value('.')
          return True
    return False


# A* search function  
# Take in the graph a visited list and wait list
# return True if find the location, False otherwise
def a_star(graph, visited, wait_list):
  global exp_nodes
  while True:
    try:
      node = wait_list.pop(0)
      # add current node to the visited list
      visited.append(node)
      if node.v == '.':
        print_path(node)
        return True
      elif node.v == '%':
        continue
      else:
        neighbor=graph_fun.get_neighbor(graph, node)
        for one in neighbor:
          if one not in visited:
            if one not in wait_list:
              # if the node hasn't be expanded yet
              # simply add it to the wait list
              exp_nodes+=1
              one.save_parent(node)
              one.update_g(node.g+1)
              one.update_f()
              wait_list.append(one)
            else:
              # if the node already in the wait list
              # update the cost to the node to the lower one
              if one.g<node.g+1:
                one.save_parent(node)
                one.update_g(node.g+1)
                one.update_f()
        # sort the wait_list according to the value of f
        # so we can visit the one with lowest f next turn
        wait_list.sort(key=operator.attrgetter('f'))
    except IndexError:
      print ("Sorry! Cannot find a path to the end!")
      return False

# Start Program
while True:
  # let user choose the maze
  maze = input("Please choose the maze you want to play:\n"
               "1. Medium Maze\n"
               "2. Large Maze\n"
               "3. Open Maze\n"
               "4. Exit\n"
               "Your choice:")
  if maze == 4:
    sys.exit()
  elif maze == 1:
    f = open('Maze/mediumMaze.txt', 'r')
  elif maze == 2:
    f = open('Maze/bigMaze.txt', 'r')
  elif maze == 3:
    f = open('Maze/openMaze.txt', 'r')
  else:
    print ("Invalid input of maze!")
    sys.exit()
  # let user choose the search method
  search_method = input("Please choose the search method:\n"
               "1. Depth-first search\n"
               "2. Breadth-first search\n"
               "3. Greedy best-first search\n"
               "4. A* search\n"
               "Your choice:")
  # using helper function to construct the graph
  graph=graph_fun.construct_graph(f)
  # print the initial graph
  graph_fun.print_graph(graph)

  # find start point and end point
  for i in range(0, len(graph)):
    for j in range(0, len(graph[0])):
      if graph[i][j].v == 'P':
        start_row = graph[i][j].r
        start_column = graph[i][j].c
      if graph[i][j].v == '.':
        end_row = graph[i][j].r
        end_column = graph[i][j].c
  # add heuristic to each node(Manhatan Distance)
  # will only be use by greedy bfs and A*
  for i in range(0, len(graph)):
    for j in range(0, len(graph[0])):
      graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column))

  # go the the specific searching function
  visited = []
  if search_method == 1:
    dfs(graph,visited,graph[start_row][start_column])
  elif search_method == 2:
    queue = deque([graph[start_row][start_column]])
    bfs(graph,visited,queue)
  elif search_method == 3:
    greedy_bfs(graph,visited,graph[start_row][start_column])
  elif search_method == 4:
    wait_list = [graph[start_row][start_column]]
    a_star(graph, visited, wait_list)
  else:
    print ("Invalid input of search method!")
    sys.exit()

  # print solved path with related data
  graph_fun.print_graph(graph)
  print "Total visited node:"+str(len(visited))
  print "Total expanded nodes"+str(exp_nodes)
  print "Path Cost:"+str(path_cost)
  exp_nodes=0
  path_cost=0



