import sys
import operator
import graph_fun
from collections import deque

path_cost=0
exp_nodes=0

# helper function to determine the next
# direction of pecman
def new_direction(old_node, new_node):
  if new_node.c == old_node.c+1:
      return "R"
  elif new_node.c == old_node.c-1:
      return "L"
  elif new_node.r == old_node.r+1:
      return "D"
  elif new_node.r == old_node.r-1:
      return "U"

# helper function to determine the cost of turn
def turn_cost(old_dir, new_dir):
  if old_dir==new_dir:
    return 0
  elif old_dir == "R":
    if new_dir == "L":
      return 2
    else:
      return 1
  elif old_dir == "L":
    if new_dir == "R":
      return 2
    else:
      return 1
  elif old_dir == "U":
    if new_dir == "D":
      return 2
    else:
      return 1
  elif old_dir == "D":
    if new_dir == "U":
      return 2
    else:
      return 1


# helper function to backtrack the path and update graph
def bfs_mark_path(node):
  global path_cost
  if node.parent:
    path_cost+=1
    node.parent.update_value('.')
    bfs_mark_path(node.parent)


# the new a* function which is basiclly the same as the
# one in 1-1 but add different cost
def a_star(search_method, graph, visited, wait_list):
  global exp_nodes
  # pecman init direction
  direction = "R"
  while True:
    try:
      node = wait_list.pop(0)
      # add current node to the visited list
      visited.append(node)
      if node.v == 'P':
        bfs_mark_path(node)
        return True
      elif node.v == '%':
        continue
      else:
        neighbor=graph_fun.get_neighbor(graph, node)
        for one in neighbor:
          if one not in visited:
            if one not in wait_list:
              # if the node hasn't be expanded yet
              # simply add it to the wait list
              exp_nodes+=1
              one.save_parent(node)
              new_dir = new_direction(node, one)
              turn = 2*turn_cost(direction, new_dir)
              if search_method==1:
                one.update_g(node.g+2+turn)
              elif search_method==2:
                one.update_g(node.g+1+2*turn)
              one.update_f()
              wait_list.append(one)
            else:
              # if the node already in the wait list
              # update the cost to the node to the lower one
              new_dir = new_direction(node, one)
              turn = 2*turn_cost(direction, new_dir)
              if search_method==1:
                if one.g>node.g+2+turn:
                  one.update_g(node.g+2+turn)
              elif search_method==2:
                if one.g>node.g+1+2*turn:
                  one.update_g(node.g+1+2*turn)
        # sort the wait_list according to the value of f
        # so we can visit the one with lowest f next turn
        wait_list.sort(key=operator.attrgetter('f'))
        continue
    except IndexError:
      print "Sorry! Cannot find a path to the end!"
      return False

# get user inputs
maze = input("Please choose the maze you want to play:\n"
             "1. Small Maze\n"
             "2. Large Maze\n"
             "Your choice:")
if maze==1:
  f = open('Maze/smallTurns.txt', 'r')
elif maze==2:
  f = open('Maze/bigTurns.txt', 'r')
search_method = input("Please choose the step cost:\n"
             "1. forward movement has cost 2 and any turn has cost 1\n"
             "2. forward movement has cost 1 and any turn has cost 2\n"
             "Your choice:")
heuristic_type = input("Please choose the heuristic:\n"
             "1. Manhatan Distance 1\n"
             "2. Informly Heuristic 2\n"
             "Your choice:")

# print init graph
graph=graph_fun.construct_graph(f)

# Find the start and end locations
for i in range(0, len(graph)):
  for j in range(0, len(graph[0])):
    if graph[i][j].v == '.':
      start_row = graph[i][j].r
      start_column = graph[i][j].c
    if graph[i][j].v == 'P':
      end_row = graph[i][j].r
      end_column = graph[i][j].c
# add different heuristic to each node according to
# the user input and the condition of each node
for i in range(0, len(graph)):
  for j in range(0, len(graph[0])):
    if heuristic_type==1:
      graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column))
    elif heuristic_type==2:
      if graph_fun.must_turn(graph, graph[i][j]):
        graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column)+20)
      if graph_fun.dead_end(graph, graph[i][j]):
        graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column)+1000)
      else:
        graph[i][j].add_heuristic(abs(graph[i][j].r - end_row)+abs(graph[i][j].c - end_column))

#start search
visited = []
wait_list = [graph[start_row][start_column]]
a_star(search_method, graph, visited, wait_list)

# print the solved path and related data
graph_fun.print_graph(graph)
print "Total visited node:"+str(len(visited))
print "Total expanded nodes"+str(exp_nodes)
print "Path Cost:"+str(path_cost)
exp_nodes=0
path_cost=0

