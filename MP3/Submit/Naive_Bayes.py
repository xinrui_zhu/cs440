__author__ = 'randy'
import sys
import math
import operator

text_file = open("spam_detection/train_email.txt", "r")
classes = text_file.read().split("\n")
spam = 0
normalEmail = 0
totalDict = {}
spamDict = {}
normalDict = {}
spamProbablity = {}
normalProbability = {}
spamWordsCount = 0
normalWordsCount = 0
wordsCount = 0

def countWords(isMultiNode):
    global spam
    global normalEmail
    global totalDict
    global spamDict
    global normalDict
    global spamWordsCount
    global normalWordsCount
    global wordsCount

    if isMultiNode:
        for c in classes:
            words = c.split(' ')

            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])
                wordsCount += int(values[1])

            if c[:1] == '0':
                normalEmail += 1

                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] in normalDict:
                        normalDict[values[0]] += int(values[1])
                    else:
                        normalDict[values[0]] = int(values[1])
                    normalWordsCount += int(values[1])
                    #print "normal words count = " + str(normalWordsCount)
            elif c[:1] == '1':
                spam += 1
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] in spamDict:
                        spamDict[values[0]] += int(values[1])
                    else:
                        spamDict[values[0]] = int(values[1])
                    spamWordsCount += int(values[1])
                    #print "spam words count = " + str(spamWordsCount)
    else:
        for c in classes:
            words = c.split(' ')

            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])
                wordsCount += int(values[1])

            if c[:1] == '0':
                normalEmail += 1
                normalSet = []
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] not in normalSet:
                        normalSet.append(values[0])
                for normalWord in normalSet:
                    if normalWord not in normalDict:
                        normalDict[normalWord] = 1
                    else:
                        normalDict[normalWord] += 1

            elif c[:1] == '1':
                spam += 1
                spamSet = []
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] not in spamSet:
                        spamSet.append(values[0])
                for spamWord in spamSet:
                    if spamWord not in spamDict:
                        spamDict[spamWord] = 1
                    else:
                        spamDict[spamWord] += 1


def constructProbDict(isMultiNode):
    global totalDict
    global spamDict
    global normalDict
    global spamProbablity
    global normalProbability

    if isMultiNode:
        for word,count in totalDict.iteritems():
            if word in spamDict:
                spamProbablity[word] = (spamDict[word] + 1.0)/(spamWordsCount + len(totalDict))
                #print " ( " + str(spamDict[word]) + " + 1.0 / ( " + str(spamWordsCount) + " + " + str(len(totalDict)) + " ) " + str(counter)


            else:
                spamProbablity[word] = 1.0/(spamWordsCount + len(totalDict))

            if word in normalDict:
                normalProbability[word] = (normalDict[word] + 1.0)/(normalWordsCount + len(totalDict))
            else:
                normalProbability[word] = 1.0/(normalWordsCount + len(totalDict))
    else:
         for word,count in totalDict.iteritems():
            if word in spamDict:
                spamProbablity[word] = (spamDict[word] + 1.0)/(350 + 2)
            else:
                spamProbablity[word] = 1.0/(350 + 2)

            if word in normalDict:
                normalProbability[word] = (normalDict[word] + 1.0)/(350 + 2)
            else:
                normalProbability[word] = 1.0/(350 + 2)





def analyzeTest(priors, probabilityList, tests, isMultiNode):
    results = []
    if isMultiNode:
        for test in tests:
            if test[:1]  == '0':
                isSpam = False
            elif test[:1] == '1':
                isSpam = True
            else:
                continue


            words = test.split(' ')
            spamProb = math.log(priors[0])
            normalProb = math.log(priors[1])
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in probabilityList[0] and values[0] in probabilityList[1]:
                    # print "spam probablity of " + values[0] + " = " + str(probabilityList[0][values[0]])
                    # print "normal probablity of " + values[0] + " = " + str(probabilityList[1][values[0]])
                    for i in range(0,int(values[1])):
                        spamProb += math.log(probabilityList[0][values[0]])
                        normalProb += math.log(probabilityList[1][values[0]])

            # print("isSpam = " + str(isSpam))
            # print("spam prob = " + str(spamProb))
            # print("norm prob = " + str(normalProb))
            if spamProb > normalProb and isSpam == True:
                results.append([True,"spam", "spam"])
            elif spamProb <= normalProb and isSpam == False:
                results.append([True,"normal", "normal"])
            elif spamProb <= normalProb and isSpam == True:
                results.append([False,"normal", "spam"])
            elif spamProb > normalProb and isSpam == False:
                results.append([False,"spam", "normal"])
    else:
         for test in tests:
            if test[:1]  == '0':
                isSpam = False
            elif test[:1] == '1':
                isSpam = True
            else:
                continue


            words = test.split(' ')
            spamProb = math.log(priors[0])
            normalProb = math.log(priors[1])
            uniqueWords = []
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] not in uniqueWords:
                    uniqueWords.append(values[0])

            for key,value in probabilityList[0].iteritems():
                if key in uniqueWords:
                    spamProb += math.log(value)
                else:
                    spamProb += math.log(1.0-value)

            for key,value in probabilityList[1].iteritems():
                if key in uniqueWords:
                    normalProb += math.log(value)
                else:
                    normalProb += math.log(1.0-value)


            if spamProb > normalProb and isSpam == True:
                results.append([True,"spam", "spam"])
            elif spamProb <= normalProb and isSpam == False:
                results.append([True,"normal", "normal"])
            elif spamProb <= normalProb and isSpam == True:
                results.append([False,"normal", "spam"])
            elif spamProb > normalProb and isSpam == False:
                results.append([False,"spam", "normal"])


    return results


isMultinomial = False
countWords(isMultinomial)

constructProbDict(isMultinomial)

test_file = open("spam_detection/test_email.txt", "r")
tests = test_file.read().split("\n")

priors = [0.5,0.5]
probList = [spamProbablity, normalProbability]

results = analyzeTest(priors,probList,tests, isMultinomial)

right = 0
wrong = 0
guessSpamGetSpam = 0
guessNormalGetNormal = 0
guessSpamGetNormal = 0
guessNormalGetSpam = 0
total = len(results)
for result in results:
    if(result[0] == True):
        right += 1
    else:
        wrong += 1

    if result[1] == "spam":
        if result[2] == "spam":
            guessSpamGetSpam += 1
        elif result[2] == "normal":
            guessSpamGetNormal += 1
    elif result[1] == "normal":
        if result[2] == "spam":
            guessNormalGetSpam += 1
        elif result[2] == "normal":
            guessNormalGetNormal += 1

print "guess spam get spam " + str(guessSpamGetSpam)
print "guess spam get normal " + str(guessSpamGetNormal)
print "guess normal get normal " + str(guessNormalGetNormal)
print "guess normal get spam " + str(guessNormalGetSpam)
print "right = " + str(right)
print "total = " + str(total)
print "Classification rate = " + str(float(right)/float(total))


counter = 1
print "Top 20 spam words"
topSpam = sorted(spamProbablity.items(), key=operator.itemgetter(1), reverse = True)
for i in range(0, 20):
    print topSpam[i]

print "Top 20 normal words"
topNormal= sorted(normalProbability.items(), key=operator.itemgetter(1), reverse = True)
for i in range(0, 20):
    print topNormal[i]
    #print str(k) + " : " + str(v)
    #counter += 1


