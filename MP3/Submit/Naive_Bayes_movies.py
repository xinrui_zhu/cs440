__author__ = 'randy'
import sys
import math
import operator

text_file = open("sentiment/rt-train.txt", "r")
classes = text_file.read().split("\n")
positive = 0
negative = 0
totalDict = {}
positiveDict = {}
negativeDict = {}
positiveProbablity = {}
negativeProbability = {}
positiveWordsCount = 0
negativeWordsCount = 0
wordsCount = 0

def countWords(isMultiNode):
    global positive
    global negative
    global totalDict
    global positiveDict
    global negativeDict
    global positiveWordsCount
    global negativeWordsCount
    global wordsCount

    if isMultiNode:
        for c in classes:
            words = c.split(' ')

            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])
                wordsCount += int(values[1])

            if c[:2] == '-1':
                negative += 1

                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] in negativeDict:
                        negativeDict[values[0]] += int(values[1])
                    else:
                        negativeDict[values[0]] = int(values[1])
                    negativeWordsCount += int(values[1])
                    #print "negative words count = " + str(negativeWordsCount)
            elif c[:1] == '1':
                positive += 1
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] in positiveDict:
                        positiveDict[values[0]] += int(values[1])
                    else:
                        positiveDict[values[0]] = int(values[1])
                    positiveWordsCount += int(values[1])
                    #print "positive words count = " + str(positiveWordsCount)
    else:
        for c in classes:
            words = c.split(' ')

            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])
                wordsCount += int(values[1])

            if c[:2] == '-1':
                negative += 1
                negativeSet = []
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] not in negativeSet:
                        negativeSet.append(values[0])
                for negativeWord in negativeSet:
                    if negativeWord not in negativeDict:
                        negativeDict[negativeWord] = 1
                    else:
                        negativeDict[negativeWord] += 1

            elif c[:1] == '1':
                positive += 1
                positiveSet = []
                for word in words:
                    if ':' not in word:
                        continue
                    values = word.split(':')
                    if values[0] not in positiveSet:
                        positiveSet.append(values[0])
                for positiveWord in positiveSet:
                    if positiveWord not in positiveDict:
                        positiveDict[positiveWord] = 1
                    else:
                        positiveDict[positiveWord] += 1


def constructProbDict(isMultiNode):
    global totalDict
    global positiveDict
    global negativeDict
    global positiveProbablity
    global negativeProbability

    if isMultiNode:
        for word,count in totalDict.iteritems():
            if word in positiveDict:
                positiveProbablity[word] = (positiveDict[word] + 1.0)/(positiveWordsCount + len(totalDict))
                #print " ( " + str(positiveDict[word]) + " + 1.0 / ( " + str(positiveWordsCount) + " + " + str(len(totalDict)) + " ) " + str(counter)


            else:
                positiveProbablity[word] = 1.0/(positiveWordsCount + len(totalDict))

            if word in negativeDict:
                negativeProbability[word] = (negativeDict[word] + 1.0)/(negativeWordsCount + len(totalDict))
            else:
                negativeProbability[word] = 1.0/(negativeWordsCount + len(totalDict))
    else:
         for word,count in totalDict.iteritems():
            if word in positiveDict:
                positiveProbablity[word] = (positiveDict[word] + 1.0)/(2000 + 2)
            else:
                positiveProbablity[word] = 1.0/(2000 + 2)

            if word in negativeDict:
                negativeProbability[word] = (negativeDict[word] + 1.0)/(2000 + 2)
            else:
                negativeProbability[word] = 1.0/(2000 + 2)





def analyzeTest(priors, probabilityList, tests, isMultiNode):
    results = []
    if isMultiNode:
        for test in tests:
            if test[:2]  == '-1':
                ispositive = False
            elif test[:1] == '1':
                ispositive = True
            else:
                continue


            words = test.split(' ')
            positiveProb = math.log(priors[0])
            negativeProb = math.log(priors[1])
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in probabilityList[0] and values[0] in probabilityList[1]:
                    # print "positive probablity of " + values[0] + " = " + str(probabilityList[0][values[0]])
                    # print "negative probablity of " + values[0] + " = " + str(probabilityList[1][values[0]])
                    for i in range(0,int(values[1])):
                        positiveProb += math.log(probabilityList[0][values[0]])
                        negativeProb += math.log(probabilityList[1][values[0]])

            # print("ispositive = " + str(ispositive))
            # print("positive prob = " + str(positiveProb))
            # print("norm prob = " + str(negativeProb))
            if positiveProb > negativeProb and ispositive == True:
                results.append([True,"positive", "positive"])
            elif positiveProb <= negativeProb and ispositive == False:
                results.append([True,"negative", "negative"])
            elif positiveProb <= negativeProb and ispositive == True:
                results.append([False,"negative", "positive"])
            elif positiveProb > negativeProb and ispositive == False:
                results.append([False,"positive", "negative"])
    else:
         for test in tests:
            if test[:2]  == '-1':
                ispositive = False
            elif test[:1] == '1':
                ispositive = True
            else:
                continue


            words = test.split(' ')
            positiveProb = math.log(priors[0])
            negativeProb = math.log(priors[1])
            uniqueWords = []
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] not in uniqueWords:
                    uniqueWords.append(values[0])

            for key,value in probabilityList[0].iteritems():
                if key in uniqueWords:
                    positiveProb += math.log(value)
                else:
                    positiveProb += math.log(1.0-value)

            for key,value in probabilityList[1].iteritems():
                if key in uniqueWords:
                    negativeProb += math.log(value)
                else:
                    negativeProb += math.log(1.0-value)


            if positiveProb > negativeProb and ispositive == True:
                results.append([True,"positive", "positive"])
            elif positiveProb <= negativeProb and ispositive == False:
                results.append([True,"negative", "negative"])
            elif positiveProb <= negativeProb and ispositive == True:
                results.append([False,"negative", "positive"])
            elif positiveProb > negativeProb and ispositive == False:
                results.append([False,"positive", "negative"])
    return results


countWords(False)

constructProbDict(False)

test_file = open("sentiment/rt-test.txt", "r")
tests = test_file.read().split("\n")

priors = [0.5,0.5]
probList = [positiveProbablity, negativeProbability]

results = analyzeTest(priors,probList,tests, False)

right = 0
wrong = 0
guesspositiveGetpositive = 0
guessnegativeGetnegative = 0
guesspositiveGetnegative = 0
guessnegativeGetpositive = 0
total = len(results)
for result in results:
    if(result[0] == True):
        right += 1
    else:
        wrong += 1

    if result[1] == "positive":
        if result[2] == "positive":
            guesspositiveGetpositive += 1
        elif result[2] == "negative":
            guesspositiveGetnegative += 1
    elif result[1] == "negative":
        if result[2] == "positive":
            guessnegativeGetpositive += 1
        elif result[2] == "negative":
            guessnegativeGetnegative += 1

print "guess positive get positive " + str(guesspositiveGetpositive)
print "guess positive get negative " + str(guesspositiveGetnegative)
print "guess negative get negative " + str(guessnegativeGetnegative)
print "guess negative get positive " + str(guessnegativeGetpositive)
print "right = " + str(right)
print "total = " + str(total)
print "Classification rate = " + str(float(right)/float(total))


counter = 1
print "Top 20 positive words"
topSpam = sorted(positiveProbablity.items(), key=operator.itemgetter(1), reverse = True)
for i in range(0, 20):
    print topSpam[i]

print "Top 20 negative words"
topNormal= sorted(negativeProbability.items(), key=operator.itemgetter(1), reverse = True)
for i in range(0, 20):
    print topNormal[i]


