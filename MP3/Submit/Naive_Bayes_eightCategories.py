__author__ = 'randy'
import sys
import math
import copy
import operator

text_file = open("8category/8category.training.txt", "r")
classes = text_file.read().split("\n")

totalDict = {}
categoryDicts = [{},{},{},{},{},{},{},{}]
probabilities = [{},{},{},{},{},{},{},{}]
categoryWordCount = [0,0,0,0,0,0,0,0]
categoryDocumentsCount = [0,0,0,0,0,0,0,0]
wordsCount = 0

def countWords(isMultiNode):
    global totalDict
    global categoryDicts
    global probabilities
    global categoryWordCount
    global wordsCount
    global categoryDocumentsCount

    if isMultiNode:
        for c in classes:
            words = c.split(' ')
            if c[:1] < '0' or c[:1] > '7':
                continue
            i = int(c[:1])

            categoryDocumentsCount[i] += 1

            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])


                if values[0] in categoryDicts[i]:
                    categoryDicts[i][values[0]] += int(values[1])
                else:
                    categoryDicts[i][values[0]] = int(values[1])
                categoryWordCount[i] += int(values[1])

                wordsCount += int(values[1])
        print categoryDocumentsCount

    else:
        for c in classes:
            words = c.split(' ')

            if c[:1] < '0' or c[:1] > '7':
                continue
            i = int(c[:1])
            categoryDocumentsCount[i] += 1


            categorySets = [set(), set(), set(), set(), set(), set(), set(), set()]
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                if values[0] in totalDict:
                    totalDict[values[0]] += int(values[1])
                else:
                    totalDict[values[0]] = int(values[1])
                wordsCount += int(values[1])
                categorySets[i].add(values[0])

            for uniqueWord in categorySets[i]:
                if uniqueWord not in categoryDicts[i]:
                    categoryDicts[i][uniqueWord] = 1
                else:
                    categoryDicts[i][uniqueWord] += 1




def constructProbDict(isMultiNode):
    global totalDict
    global categoryDicts
    global probabilities
    global categoryWordCount
    global wordsCount
    global categoryDocumentsCount

    if isMultiNode:
        for word,count in totalDict.iteritems():
            for i in range(0,len(categoryDicts)):
                if word in categoryDicts[i]:
                    probabilities[i][word] = (categoryDicts[i][word] + 1.0)/(categoryWordCount[i] + len(totalDict))
                    #print " ( " + str(positiveDict[word]) + " + 1.0 / ( " + str(positiveWordsCount) + " + " + str(len(totalDict)) + " ) " + str(counter)
                else:
                     probabilities[i][word] =  1.0/(categoryWordCount[i] + len(totalDict))

    else:
         for word,count in totalDict.iteritems():
            for i in range(0,len(categoryDicts)):
                if word in categoryDicts[i]:
                    probabilities[i][word] = (categoryDicts[i][word] + 1.0)/(categoryDocumentsCount[i] + len(categoryDicts))
                else:
                    probabilities[i][word] = 1.0/(categoryDocumentsCount[i] + len(categoryDicts))





def analyzeTest(priors, probabilityList, tests, isMultiNode, confusionMatrix):
    results = []
    if isMultiNode:
        for test in tests:
            if test[:1] < '0' or test[:1] > '7':
                continue
            answer = int(test[:1])


            words = test.split(' ')
            prob = copy.deepcopy(priors)
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                toContitnue = True

                for dict in probabilityList:
                    if values[0] not in dict:
                        toContitnue = False
                if not toContitnue:
                    continue

                for i in range(0,int(values[1])):
                    for j in range(0, len(probabilityList)):
                        prob[j] += math.log(probabilityList[j][values[0]])

            if prob.index(max(prob)) == answer:
                results.append(True)
            else:
                results.append(False)
            # print "guess = " + str(prob.index(max(prob)))
            # print "answer = " + str(answer)
            # print prob
            confusionMatrix[prob.index(max(prob))][answer] += 1

    else:
         for test in tests:
            if test[:1] < '0' or test[:1] > '7':
                continue
            answer = int(test[:1])



            words = test.split(' ')
            prob = copy.deepcopy(priors)
            uniqueWords = set()
            for word in words:
                if ':' not in word:
                    continue
                values = word.split(':')
                uniqueWords.add(values[0])

            for i in range(0,len(probabilityList)):
                for key,value in probabilityList[i].iteritems():
                    if key in uniqueWords:
                        prob[i] += math.log(value)
                    else:
                        prob[i] += math.log(1.0-value)

            if prob.index(max(prob)) == answer:
                results.append(True)
            else:
                results.append(False)
            confusionMatrix[prob.index(max(prob))][answer] += 1
    return results

isMultiNode = False
countWords(isMultiNode)

constructProbDict(isMultiNode)

test_file = open("8category/8category.testing.txt", "r")
tests = test_file.read().split("\n")

priors = copy.deepcopy(categoryDocumentsCount)
for i in range(0, len(priors)):
    priors[i] /= 1900.0
    priors[i] = math.log(priors[i])

print priors
probList = probabilities

confusionMatrix = [[0 for i in range(8)] for j in range(8)]

results = analyzeTest(priors,probList,tests, isMultiNode, confusionMatrix)

right = 0
wrong = 0
total = len(results)
for result in results:
    if(result == True):
        right += 1
    else:
        wrong += 1


print "right = " + str(right)
print "total = " + str(total)
print "Classification rate = " + str(float(right)/float(total))

for i in range(0,len(probabilities)):
    print "Top 20 words for category " + str(i)
    topSpam = sorted(probabilities[i].items(), key=operator.itemgetter(1), reverse = True)
    for i in range(0, 20):
        print topSpam[i]


for i in range(len(confusionMatrix)):
    for j in range(len(confusionMatrix[0])):
        print confusionMatrix[j][i],
        print ' ',
    print "\n"


