import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

class pixel(object):
	"""docstring for pixel"""
	def __init__(self, smooth_k, foreground, background):
		self.foreground = foreground
		self.background = background
		self.total = smooth_k
	def add_foreground(self):
		self.total += 1
		self.foreground = round((float(self.foreground)*(self.total-1)+100)/self.total, 2)
		self.background = round((float(self.background)*(self.total-1)+0)/self.total, 2)
	def add_background(self):
		self.total += 1
		self.foreground = round((float(self.foreground)*(self.total-1)+0)/self.total, 2)
		self.background = round((float(self.background)*(self.total-1)+100)/self.total, 2)

def print_image_prob(image):
	for i in range(0,70):
		for j in range(0,60):
			print image[i][j].foreground,
		print '\n'

def init_an_image():
	image = []
	for i in range(0, 70):
		line_list = []
		for j in range(0, 60):
			new_p = pixel(20,50,50)
			line_list.append(new_p)
		image.append(line_list)
	# print_image_prob(image)
	return image

def update_image(f, image):
	for i in range(0, 70):
		line = f.readline()
		if line=='':
			return False
		line = line.strip('\n')
		# print line
		line_list = image[i]
		for j in range(0, 60):
			if line[j]==' ':
				line_list[j].add_background()
			else:
				line_list[j].add_foreground()

def compare_image(f, face, not_face, p_face, p_not_face, num, label):
	global face_max_prob
	global face_max
	global face_min_prob
	global face_min
	global not_face_max_prob
	global not_face_max
	global not_face_min_prob
	global not_face_min
	p0 = math.log(p_not_face)
	p1 = math.log(p_face)
	for i in range(0, 70):
		line = f.readline()
		if line=='':
			return False
		line = line.strip('\n')
		for j in range(0, 60):
			if line[j] ==' ':
				p0 += math.log(not_face[i][j].background)
				p1 += math.log(face[i][j].background)
			else:
				p0 += math.log(not_face[i][j].foreground)
				p1 += math.log(face[i][j].foreground)
	if p1>=p0:
		prob = 1
	else:
		prob = 0
	if label == 0:
		if p0>not_face_max_prob:
			not_face_max_prob = p0
			not_face_max = num
		if p0<not_face_min_prob:
			not_face_min_prob = p0
			not_face_min = num
	else:
		if p1>face_max_prob:
			face_max_prob = p1
			face_max = num
		if p1<face_min_prob:
			face_min_prob = p1
			face_min = num
	return prob


def generate_img(img):
	image = []
	for i in range(0,70):
		line = []
		for j in range(0,60):
			foreground = img[i][j].foreground/70
			new_pixel = [0,foreground,0]
			line.append(new_pixel)
		image.append(line)
	return image

def print_odd_ratio(img1, img2):
	image = []
	for i in range(0,28):
		line = []
		for j in range(0,28):
			foreground = float(img1[i][j].foreground)/img2[i][j].foreground
			new_pixel = [img1[i][j].foreground/50,img2[i][j].foreground/50,0]
			line.append(new_pixel)
		image.append(line)
	return image

# Program Start
face = init_an_image()
not_face = init_an_image()
face_num = 0
not_face_num = 0
total = 0
f1 = open("facedata/facedatatrain")
f2 = open("facedata/facedatatrainlabels")
while True:
	face_or_not = f2.readline().strip('\n')
	if face_or_not == '':
		break
	if face_or_not == '1':
		face_num+=1
		total+=1
		update_image(f1, face)
	else:
		not_face_num+=1
		total+=1
		update_image(f1, not_face)
# print "face"
# # print_image_prob(face)
# color_image = generate_img(face)
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# print "not face"
# print_image_prob(not_face)
# color_image = generate_img(not_face)
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
p_face = 100*float(face_num)/total
p_not_face = 100*float(not_face_num)/total
# color_image = print_odd_ratio(face, not_face)
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
print p_face
print p_not_face
f3 = open("facedata/facedatatest")
f4 = open("facedata/facedatatestlabels")
f5 = open("faceresult", 'w')
correct = 0
wrong = 0
face_correct = 0
total_face = 0
not_face_correct = 0
total_not_face = 0
face_max_prob = 0
face_max = 0
face_min_prob = 'inf'
face_min = 0
not_face_max_prob = 0
not_face_max = 0
not_face_min_prob = 'inf'
not_face_min = 0
count = 0
while True:
	label = f4.readline()
	if label == '':
		break
	else:
		label = int(label.strip('\n'))
		count+=1
		prob = compare_image(f3, face, not_face, p_face, p_not_face, count, label)
		f5.write(str(prob)+'\n')
		if prob == label:
			correct +=1
			if label == 1:
				face_correct += 1
				total_face +=1
			else:
				not_face_correct +=1
				total_not_face +=1
		else:
			wrong+=1
			if label == 1:
				total_face +=1
			else:
				total_not_face +=1
print "accuracy:"+str(float(correct)/150)
print "correct face:"+str(face_correct)+'/'+str(total_face)
print "correct not face:"+ str(not_face_correct)+'/'+str(total_not_face)
print "face max:"+str(face_max)+"   face min:"+str(face_min)
print "not face max:"+str(not_face_max)+"   not face min:"+str(not_face_min)
