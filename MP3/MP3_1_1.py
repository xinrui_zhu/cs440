import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import array

class pixel(object):
	"""docstring for pixel"""
	def __init__(self, smooth_k, foreground, background):
		self.foreground = foreground
		self.background = background
		self.total = smooth_k
	def add_foreground(self):
		self.total += 1
		self.foreground = round((float(self.foreground)*(self.total-1)+100)/self.total, 2)
		self.background = round((float(self.background)*(self.total-1)+0)/self.total, 2)
	def add_background(self):
		self.total += 1
		self.foreground = round((float(self.foreground)*(self.total-1)+0)/self.total, 2)
		self.background = round((float(self.background)*(self.total-1)+100)/self.total, 2)
		
class number(object):
	"""docstring for number"""
	def __init__(self, pixel_map, value):
		self.pixel_map = pixel_map
		self.value = value

def print_image_prob(image):
	for i in range(0,28):
		for j in range(0,28):
			# prob = math.log(image[i][j].foreground)
			# if prob<0:
			# 	print '-',
			# elif prob<=2:
			# 	print ' ',
			# else:
			# 	print '+',
			print image[i][j].foreground/100,
		print '\n'

def print_odd_ratio(img1, img2):
	image = []
	for i in range(0,28):
		line = []
		for j in range(0,28):
			foreground = float(img1[i][j].foreground)/img2[i][j].foreground
			print foreground/5,
			new_pixel = [img1[i][j].foreground/100,img2[i][j].foreground/100,0]
			line.append(new_pixel)
		image.append(line)
		print '\n'
	return image

def print_image(image):
	for i in range(0,28):
		for j in range(0,28):
			if image[i][j].foreground == 100:
				print '#',
			else:
				print ' ',
		print '\n'

def save_prob(image, num):
	f = open("trainingresult"+str(num), 'w')
	for i in range(0,28):
		for j in range(0,28):
			f.write(image[i][j].foreground)
			f.write(' ')
		f.write('\n')

def read_an_image_smooth(f):
	image = []
	for i in range(0, 28):
		line = f.readline()
		if line=='':
			return False
		line = line.strip('\n')
		line_list = []
		for j in range(0, 28):
			new_p = pixel(40,50,50)
			if line[j] == ' ':
				new_p.add_background
			else:
				new_p.add_background
			line_list.append(new_p)
		image.append(line_list)
	# print_image_prob(image)
	return image

def read_an_image(f):
	image = []
	for i in range(0, 28):
		line = f.readline()
		if line=='':
			return False
		line = line.strip('\n')
		line_list = []
		for j in range(0, 28):
			if line[j] == ' ':
				new_p = pixel(1,0,100)
			else:
				new_p = pixel(1,100,0)
			line_list.append(new_p)
		image.append(line_list)
	# print_image_prob(image)
	return image

def update_image(f, image):
	for i in range(0, 28):
		line = f.readline()
		if line=='':
			return False
		line = line.strip('\n')
		# print line
		line_list = image[i]
		for j in range(0, 28):
			if line[j]==' ':
				line_list[j].add_background()
			else:
				line_list[j].add_foreground()

# def compare_image(img1, img2):
# 	diff = 0
# 	for i in range(0, 28):
# 		for j in range(0, 28):
# 			diff += pow(abs(img1[i][j].foreground - img2[i][j].foreground),2)+pow(abs(img1[i][j].background - img2[i][j].background),2)
# 	return diff


def compare_image(img1, img2, p_class):
	p = math.log(p_class)
	for i in range(0, 28):
		for j in range(0, 28):
			if img1[i][j].foreground == 0:
				new_p = math.log(img2[i][j].background)
			else:
				new_p = math.log(img2[i][j].foreground)
			p+=new_p
	return p

def generate_img(img):
	image = []
	for i in range(0,28):
		line = []
		for j in range(0,28):
			foreground = img[i][j].foreground/100
			new_pixel = [0,foreground,0]
			line.append(new_pixel)
		image.append(line)
	return image


#Program Start
training_list = [None]*10
training_class = [0]*10
p_class = [0]*10
f1 = open("digitdata/trainingimages")
f2 = open("digitdata/traininglabels") 
while True:
	num = f2.readline().strip('\n')
	if num=='':
		break
	else:
		num = int(num)
		training_class[num]+=1
		if training_list[num]==None:
			new_image=read_an_image_smooth(f1)
			training_list[num] = new_image
		else:
			update_image(f1, training_list[num])
for i in range(0, 10):
	p_class[i] = round((float(training_class[i])/5000),4)*100
	print i
	# print_image_prob(training_list[i])
	# print p_class[i]
	# image = training_list[i]
	# print_image_prob(image)
	# save_prob(image,i)

# generate the probability weighted image
# color_image = generate_img(training_list[4])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# color_image = generate_img(training_list[7])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# color_image = generate_img(training_list[5])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# color_image = generate_img(training_list[8])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()

# print "9 vs 4"
# color_image = print_odd_ratio(training_list[9],training_list[4])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# print "9 vs 7"
# color_image = print_odd_ratio(training_list[9],training_list[7])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# print "3 vs 8"
# color_image = print_odd_ratio(training_list[3],training_list[8])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()
# print "3 vs 5"
# color_image = print_odd_ratio(training_list[3],training_list[5])
# arr = np.array(color_image, dtype=np.float32)
# imgplot = plt.imshow(arr)
# plt.show()



f3 = open("digitdata/testimages")
f4 = open("digitdata/testlabels")
f5 = open("digitdata/testresult", 'w')
confusion_matrix = []
max_prob_list = [0]*10
max_prob_img = [0]*10
min_prob_list = ['inf']*10
min_prob_img = [0]*10
for i in range(0,10):
	new_line = [0]*10
	confusion_matrix.append(new_line)
total_diff = 0
for k in range(0, 1000):
	test_image = read_an_image(f3)
	real_value = f4.readline().strip('\n')
	real_value = int(real_value)
	max_prob = 0
	prob_num = 0
	for i in range(0, 10):
		new_prob = compare_image(test_image, training_list[i], p_class[i])
		if i==real_value:
			if new_prob>max_prob_list[real_value]:
				max_prob_list[real_value] = new_prob
				max_prob_img[real_value] = k
			if new_prob<min_prob_list[real_value]:
				min_prob_list[real_value]=new_prob
				min_prob_img[real_value] = k
		if new_prob>max_prob:
			max_prob = new_prob
			prob_num = i
	f5.write(str(prob_num)+'\n')
	confusion_matrix[real_value][prob_num] += 1
	if real_value!=prob_num:
		total_diff+=1
		print "real_value:"+str(real_value)+"  prob_num:"+str(prob_num)+"  total_diff:"+str(total_diff)+'\n'
	# print "real_value:"+str(real_value)+"  prob_num:"+str(prob_num)+"  total_diff:"+str(total_diff)+'\n'
for m in range(0, 10):
	print m
	print max_prob_img[m]
	print min_prob_img[m]
print total_diff
print confusion_matrix
