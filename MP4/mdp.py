import helper
world = helper.construct_world()
helper.print_world(world)


def grab_actions(world, x, y):
	action = [None]*6  # 1, 5:up, 2:right, 3: down, 4, 0: left
	curr_state = world[y][x]
	if y>0:
		action[1] = world[y-1][x]
		action[5] = world[y-1][x]
		if y<5:
			action[3] = world[y+1][x]
		else:
			action[3] = None
	else:
		action[1] = None
		action[5] = None
	if x>0:
		action[0] = world[y][x-1]
		action[4] = world[y][x-1]
		if x<5:
			action[2] = world[y][x+1]
		else:
			action[2] = None
	else:
		action[0] = None
		action[4] = None
	return action



def calculat_utility(next_s, s, pos):
	if next_s == None or next_s.is_wall:
		return pos*s.utility
	else:
		return pos*next_s.utility


def value_iteration(world):
	for y in range(0, 6):
		for x in range(0,6):
			curr_state = world[y][x]
			reward = curr_state.reward
			if curr_state.is_terminal:
				curr_state.update_utility = reward
			# set the actions for utility calculation
			else:
				action = grab_actions(world, x, y)
				max_weighted_utility = -10000
				policy = None
				for i in range(1, 5):
					weighted_utility = (calculat_utility(action[i], curr_state, 0.8)
										+calculat_utility(action[i-1], curr_state, 0.1)
										+calculat_utility(action[i+1], curr_state, 0.1))
					if weighted_utility>max_weighted_utility:
						max_weighted_utility = weighted_utility
						policy = i
				curr_state.update_utility(reward+0.99*max_weighted_utility)
				curr_state.update_policy(policy)
	for y in range(0, 6):
		for x in range(0,6):
			world[y][x].restore_utility()
	helper.print_utility(world)

def policy_iteration(world):
	changed = False
	for y in range(0, 6):
		for x in range(0,6):
			curr_state = world[y][x]
			curr_policy = curr_state.policy
			curr_weighted_utility = (calculat_utility(action[curr_policy], curr_state, 0.8)
									+calculat_utility(action[curr_policy-1], curr_state, 0.1)
									+calculat_utility(action[curr_policy+1], curr_state, 0.1))
			policy = None
			for i in range(1, 5):
					weighted_utility = (calculat_utility(action[i], curr_state, 0.8)
										+calculat_utility(action[i-1], curr_state, 0.1)
										+calculat_utility(action[i+1], curr_state, 0.1))
					if weighted_utility>curr_weighted_utility:
						curr_weighted_utility = weighted_utility
						policy = i
			if policy!=curr_policy:
				curr_state.update_utility(reward+0.99*max_weighted_utility)
				curr_state.update_policy(policy)
				changed = True




helper.print_utility(world)
for i in range(0,10):
	value_iteration(world)




