# this file contains the class for the world
# and the helper function to construct the world

class world_square(object):
	"""docstring for world_square"""
	def __init__(self, is_wall, is_start, is_terminal, reward, utility):
		self.is_wall = is_wall
		self.is_start = is_start
		self.is_terminal = is_terminal
		self.reward = reward
		self.utility = utility
		self.new_utility = 0
		self.policy = 1
	def update_utility(self, new_utility):
		self.new_utility = new_utility
	def restore_utility(self):
		self.utility = self.new_utility
		self.new_utility = 0
	def update_policy(self, new_policy):
		self.policy = new_policy
		
def print_world(world):
	for each_row in world:
		for each_cell in each_row:
			if each_cell.is_start:
				print " start ",
			elif each_cell.is_wall:
				print " wall ",
			else:
				print " "+str(each_cell.reward)+" ",
		print '\n'

def print_utility(world):
	for y in range(0, 6):
		for x in range(0, 6):
			print world[y][x].utility,
		print '\n'

def construct_world():
	world = []
	for i in range(0,6):
		row = [None]*6
		world.append(row)
	world[0][0] = world_square(False, False, False, -0.04,0)
	world[1][0] = world_square(False, False, False, -0.04,0)
	world[2][0] = world_square(False, False, False, -0.04,0)
	world[3][0] = world_square(False, False, False, -0.04,0)
	world[4][0] = world_square(False, False, False, -0.04,0)
	world[5][0] = world_square(False, False, True, 1,0)
	world[0][1] = world_square(False, False, True, -1,0)
	world[1][1] = world_square(False, False, False, -0.04,0)
	world[2][1] = world_square(False, False, False, -0.04,0)
	world[3][1] = world_square(False, True, False, -0.04,0)
	world[4][1] = world_square(False, False, False, -0.04,0)
	world[5][1] = world_square(False, False, True, -1,0)
	world[0][2] = world_square(False, False, False, -0.04,0)
	world[1][2] = world_square(False, False, False, -0.04,0)
	world[2][2] = world_square(False, False, False, -0.04,0)
	world[3][2] = world_square(False, False, False, -0.04,0)
	world[4][2] = world_square(False, False, False, -0.04,0)
	world[5][2] = world_square(False, False, False, -0.04,0)
	world[0][3] = world_square(False, False, False, -0.04,0)
	world[1][3] = world_square(True, False, False, 0,0)
	world[2][3] = world_square(True, False, False, 0,0)
	world[3][3] = world_square(True, False, False, 0,0)
	world[4][3] = world_square(False, False, False, -0.04,0)
	world[5][3] = world_square(True, False, False, 0,0)
	world[0][4] = world_square(False, False, False, -0.04,0)
	world[1][4] = world_square(False, False, True, -1,0)
	world[2][4] = world_square(False, False, False, -0.04,0)
	world[3][4] = world_square(False, False, False, -0.04,0)
	world[4][4] = world_square(False, False, False, -0.04,0)
	world[5][4] = world_square(False, False, True, -1,0)
	world[0][5] = world_square(False, False, False, -0.04,0)
	world[1][5] = world_square(False, False, False, -0.04,0)
	world[2][5] = world_square(False, False, True, 3,0)
	world[3][5] = world_square(False, False, False, -0.04,0)
	world[4][5] = world_square(False, False, False, -0.04,0)
	world[5][5] = world_square(False, False, True, -1,0)
	return world



