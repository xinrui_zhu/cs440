search order: index order of the letters in the result array
root -> A -> N -> A -> L -> Backtrack
                  O -> L -> Backtrack
             P -> E -> L -> Backtrack
                  F -> L -> Backtrack
             S -> A -> L -> Backtrack
                  H -> L -> Backtrack
                  U -> L -> P -> E -> A -> Success: result is:ASULPEA
                                 I -> E -> Success: result is:ASULPIE
        B -> A -> A -> O -> Backtrack
                  C -> O -> Backtrack
                  H -> O -> Backtrack
                  Y -> O -> Backtrack
             E -> E -> O -> Backtrack
             U -> G -> O -> Backtrack
        E -> E -> E -> F -> Backtrack
             M -> A -> F -> Backtrack
                  E -> F -> Backtrack
             W -> H -> F -> Backtrack
                  O -> F -> Backtrack
        D -> A -> A -> A -> Backtrack
                  C -> A -> Backtrack
                  H -> A -> Backtrack
                  Y -> A -> Backtrack
             O -> H -> A -> Backtrack
                  O -> A -> Backtrack
             Z -> A -> A -> Backtrack
                  Z -> A -> Backtrack
        G -> A -> A -> A -> Backtrack
                  C -> A -> Backtrack
                  H -> A -> Backtrack
                  Y -> A -> Backtrack
             N -> A -> A -> Backtrack
                  O -> A -> Backtrack
        J -> A -> A -> U -> Backtrack
                  C -> U -> Backtrack
                  H -> U -> Backtrack
                  Y -> U -> Backtrack
        M -> O -> H -> E -> Backtrack
                  O -> E -> Backtrack
        P -> I -> C -> E -> Backtrack
                       U -> Backtrack
             U -> G -> E -> Backtrack
                       U -> Backtrack
        R -> A -> A -> I -> Backtrack
                       O -> Backtrack
                  C -> I -> Backtrack
                       O -> Backtrack
                  H -> I -> Backtrack
                       O -> Backtrack
                  Y -> I -> Backtrack
                       O -> Backtrack
        T -> E -> E -> E -> Backtrack
                       O -> Backtrack
             O -> H -> E -> Backtrack
                       O -> Backtrack
                  O -> E -> Backtrack
                       O -> Backtrack
        Y -> A -> A -> A -> Backtrack
                  C -> A -> Backtrack
                  H -> A -> Backtrack
                  Y -> A -> Backtrack
        Z -> H -> A -> H -> Backtrack
                  E -> H -> Backtrack
                  M -> H -> Backtrack
                  O -> H -> Backtrack
                  U -> H -> Backtrack
