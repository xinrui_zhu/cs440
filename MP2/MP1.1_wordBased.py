import operator
from types import *

filelist = ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt', 'puzzle4.txt', 'puzzle5.txt']

# parser to read the file given
def problemParser(filename):
    dictFile = dict()
    with open(filename,'r') as inf:
        numTile = 0
        for i, line in enumerate(inf):
            if(i == 0):
                numTile = int(line.split()[0])
                continue
        
            line = line.replace(",", "")
            line = line.replace(":", "")
            
            value = []
            for j, word in enumerate(line.split()):
                if(j == 0):
                    key = word
                else:
                    value.append(word)
            dictFile[key] =  value   
    return numTile, dictFile

def wordParser(filename):
    dictFile = dict()
    with open(filename,'r') as inf:
        for i, line in enumerate(inf):
            line = line.replace(",", "")
            line = line.replace(":", "")
            
            value = []
            for j, word in enumerate(line.split()):
                if(j == 0):
                    key = word
                else:
                    value.append(word)
            dictFile[key] =  value   
    return dictFile

    
def findCandidates(revisedProblem, wordlist, currIndex):
    types = revisedProblem[currIndex]
    letterSet = [set() for _ in range(len(types))]
    
    for i, combination in enumerate(types):
        wordtype = combination[0]
        index = combination[1]
        for word in wordlist[wordtype]:
            if(word[index] not in letterSet[i]):
                letterSet[i].add(word[index])
    
    return list(set.intersection(letterSet))

def testConstraints(word, currlist, indexes):
    for i in range(0,3):    
        if(currlist[int(indexes[i])-1] is not ' ' and currlist[int(indexes[i])-1] is not word[i]):
            return False
    
    return True

def alphabeticSearch(problem, currlist, currType, totalTypes, searchOrder):
    # print currType
    # print totalTypes
    if(currType == totalTypes):
        print currType
        print totalTypes
        string = ''.join(currlist)
        print currlist
        searchTrace.append(list(searchList))
        searchTrace.append('Success: result is:'+string)
        return True
    
    wordType = searchOrder[currType][0]
    indexes = problem[wordType]
    dead_end = True
    for word in wordlist[wordType]:
        modifiedIndex = []
        searchList.append(word)
        if  testConstraints(word, currlist, indexes):
            dead_end = False
            # record the updated indexes
            for i in range(0,3):    
                if(currlist[int(indexes[i])-1] is ' '):
                    modifiedIndex.append(int(indexes[i])-1)
                    currlist[int(indexes[i])-1] = word[i]
            alphabeticSearch(problem, currlist, currType + 1, totalTypes, searchOrder)
        searchList.pop()
        for i in modifiedIndex:
            currlist[i] = ' '
    if dead_end==True:
        searchTrace.append(list(searchList))
        searchTrace.append('Backtrace')
            
    return True

def fileoutput(searchTrace):
    file = open('puzzle5_wordBased.txt', 'w')
    order_string = ""
    for each in searchOrder:
        if order_string!="":
            order_string += " -> " + each[0]
        else:
            order_string += each[0]

    file.write("search order: " + order_string + "\n")
    pre_row = ""
    current_row = ""
    for search in searchTrace:
        if type(search) is ListType: 
            variation_string = "root -> "  
            for word in search:
                variation_string += word + ' -> '
            current_row=variation_string
            count_same = 0
            # pdb.set_trace()
            for x in range(0,min(len(pre_row), len(current_row))):
                if pre_row[x] == current_row[x]:
                    count_same+=1
                else:
                    break
            variation_string=variation_string.replace(pre_row[0:count_same], ' '*count_same)
            pre_row=current_row
        elif type(search) is StringType:
            variation_string = search + '\n'
        file.write(variation_string)
    file.close()

# Program Start
wordlist = wordParser('wordlist.txt')
numTile, problem = problemParser('puzzles/puzzle5.txt')
# print problem
searchDict = dict()
for key in problem.keys():
    searchDict[key] = len(wordlist[key])
# print searchDict
searchOrder = sorted(searchDict.items(), key = operator.itemgetter(1))
# print searchOrder

searchTrace = []
searchList = []
currlist = [' ' for _ in range(numTile)]
alphabeticSearch(problem, currlist, 0, len(searchOrder), searchOrder)
# print searchTrace
fileoutput(searchTrace)