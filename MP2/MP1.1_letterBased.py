import pdb
from types import *
filelist = ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt', 'puzzle4.txt', 'puzzle5.txt']

# parser to read the file given
def fileParser(filename):
    dictFile = dict()
    with open(filename,'r') as inf:
        for i, line in enumerate(inf):
            if(i == 0):
                continue
        
            line = line.replace(",", "")
            line = line.replace(":", "")
            
            value = []
            i = 0
            for j, word in enumerate(line.split()):
                if(j == 0):
                    key = word
                else:
                    value.append(word)
            dictFile[key] =  value   
    return dictFile


def mostConstrainVarriable(problem, numOfSpot):
    value = []
    indexCount = [0 for x in range(numOfSpot)]
    
    for pair in problem:
        value = pair[1]
        for i in value:
            i = int(i) - 1
            indexCount[i] = indexCount[i] + 1
        
    return indexCount.index(max(indexCount)) + 1
    
   
 # find all of the possible letter at current Index
def findCandidates(revisedProblem, currIndex):
    # pdb.set_trace()
    global wordlist
    # types [('type_name', index in the word)...]
    types = revisedProblem[currIndex]
    # letterSet : [set(), set()...]
    letterSet = [set() for _ in range(len(types))]
    # print letterSet
    for i, combination in enumerate(types):
        wordtype = combination[0]
        index = combination[1]
        for word in wordlist[wordtype]:
            if(word[index] not in letterSet[i]):
                letterSet[i].add(word[index])
    if len(types) <2:
        return list(letterSet[0])
    else:
        for j in range (1, len(types)):
            letterSet[0].intersection_update(letterSet[j])
    # pdb.set_trace()
    return list(letterSet[0])

def testConstraints(problem, wordType, currlist):
    global wordlist
    indexes = problem[wordType]
    words = wordlist[wordType] 
    startingLetters = ''
    # if there's no words starting with the letters in current list return false
    for index in indexes:
        if(currlist[int(index)-1] != ' '):
            startingLetters = startingLetters + currlist[int(index)-1]
        else:
            continue

    for word in words:
        if(word.startswith(startingLetters)):
            return True
    
    return False

def alphabeticSearch(problem, revisedProblem, currlist, currIndex, totalLength):
    global searchTrace
    global searchList
    
    letterSet = findCandidates(revisedProblem, currIndex)
    available = True
    dead_end = True
    for letter in letterSet:
        searchList.append(letter)
        currlist[currIndex] = letter
        constraints = revisedProblem[currIndex]
        for constraint in constraints:
            wordType = constraint[0]
            if not(testConstraints(problem, wordType, currlist)):
                available = False
                break
            else:
                available = True
        if available == True:
            dead_end = False
            if currIndex == totalLength-1:
                string = ''.join(searchList)
                # print string
                searchTrace.append(list(searchList))
                searchTrace.append('Success: result is:'+string)
            else:
                alphabeticSearch(problem, revisedProblem, currlist, currIndex + 1, totalLength)
        searchList.pop()
        currlist[currIndex] = ' '
    if dead_end == True:
        string = ''.join(searchList)
        # print string
        searchTrace.append(list(searchList))
        searchTrace.append('Backtrack')
    return True       

def fileoutput(searchTrace):
    file = open('puzzle5_letterBsed.txt', 'w')
    file.write("search order: index order of the letters in the result array\n")
    pre_row = ""
    current_row = ""
    for search in searchTrace:
        if type(search) is ListType: 
            variation_string = "root -> "  
            for letter in search:
                variation_string += letter + ' -> '
            current_row=variation_string
            count_same = 0
            # pdb.set_trace()
            for x in range(0,min(len(pre_row), len(current_row))):
                if pre_row[x] == current_row[x]:
                    count_same+=1
                else:
                    break
            variation_string=variation_string.replace(pre_row[0:count_same], ' '*count_same)
            pre_row=current_row
        elif type(search) is StringType:
            variation_string = search + '\n'
        file.write(variation_string)
    file.close()

# program starts
wordlist = fileParser('wordlist.txt')
print wordlist
problem = fileParser('puzzles/puzzle5.txt')
print problem
infile = open('puzzles/puzzle5.txt', 'r')
numTile = int(infile.readline())
searchTrace = []
searchList = []
currlist = [' ' for _ in range(numTile)]

revisedProblem = dict()
for x in range(numTile):
    revisedProblem[x] = []

for key, value in problem.items():
    for i, value in enumerate(value):
        revisedProblem[int(value) - 1].append((key, i)) 

# print(revisedProblem)

alphabeticSearch(problem, revisedProblem, currlist, 0, numTile)
# print searchTrace
fileoutput(searchTrace)
