import generate_graph
import matplotlib.pyplot as plt

dot_color = {'R': 'ro', 'B': 'bo', 'G': 'go', 'Y': 'yo'}
def test_color(points_list):
	for each_point in points_list:
		for each_edge in each_point.edges:
			if (each_edge.color!='') and (each_point.color!='') and (each_edge.color == each_point.color):
				# print "False"
				return False
	# print "True"
	return True

def color_map(point_index):
	curr_point = points_list[point_index]
	while curr_point.color_candidate:
		curr_color = curr_point.pop_color()
		curr_point.update_color(curr_color)
		if test_color(points_list):
			if point_index == len(points_list) - 1:
				return True
			else:
				if color_map(point_index +1):
					return True
				else:
					# try the next color candidate
					continue
		else:
			continue
	# If all of the possibility are not a solution
	# restore the color_candidate and color for next test
	curr_point.restore_color()
	return False

def test_color_backtrack(points_list, point_index):
	for each_point in points_list:
		for each_edge in each_point.edges:
			if (each_edge.color!='') and (each_point.color!='') and (each_edge.color == each_point.color):
				# print "False"
				return False
	# print "True"
	color = points_list[point_index].color
	for each_edge in points_list[point_index].edges:
		each_edge.delete_candidate(color)
	dead_end = False
	for each_edge in points_list[point_index].edges:
		if not each_edge.color_candidate:
			dead_end = True
			break
	if dead_end:
		for each_edge in points_list[point_index].edges:
			each_edge.add_back_candidate()
		return False
	return True


def color_map_backtrack(point_index):
	curr_point = points_list[point_index]
	while curr_point.color_candidate:
		curr_color = curr_point.pop_color()
		curr_point.update_color(curr_color)
		if test_color_backtrack(points_list, point_index):
			if color_map(point_index+1):
				for each_edge in points_list[point_index].edges:
					each_edge.add_back_candidate()
				return True
			else:
				continue
		else:
			continue
	curr_point.restore_color()
	return False

points_list = generate_graph.graph(50)
if color_map(0):
	# generate_graph.image(points_list)
	for each_point in points_list:
		plt.plot(each_point.x, each_point.y, dot_color[each_point.color])
		for each_edge in each_point.edges:
			plt.plot([each_point.x, each_edge.x], [each_point.y, each_edge.y], 'black')
	plt.axis([-5, 55, -5, 55])
	plt.show()
else:
	print "This graph cannot be colored with 4 colors"
