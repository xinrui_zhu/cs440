from random import randint
from math import *
import operator
import copy
import pdb
import matplotlib.pyplot as plt

class Point(object):
	"""docstring for point"""
	edges = []
	edge_candidate = []
	color = ''
	color_candidate = ['R', 'Y', 'B', 'G']
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def restore_color():
		self.color_candidate = ['R', 'Y', 'B', 'G']
		color = ''


def calculate_distance(x1,y1,x2,y2):
	dist = sqrt(pow(x1-x2, 2) + pow(y1-y2, 2))
	return dist

def ccw(A,B,C):
	return (C.y-A.y)*(B.x-A.x) > (B.y-A.y)*(C.x-A.x)

def intersect(A,B,C,D):
	return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def two_seg_intersect(p1, p2, q1, q2):
	d = (q2.y-q1.y) * (p2.x-p1.x) - (q2.x-q1.x) * (p2.y - p1.y)
	n_a = (q2.x-q1.x) * (p1.y - q1.y) - (q2.y - q1.y) * (p1.x - q1.x)
	n_b = (p2.x - p1.x) * (p1.y - q1.y) - (p2.y - p1.y) * (p1.x - q1.x)
	# if n_a =n_b=0, these two segments are coincidental
	if n_a==0 and n_b==0:
		return True
	# if d=0 the two line are parallel
	if d==0:
		return False
	# Calculate the intersect point
	u_a = n_a/d
	u_b = n_b/d
	new_x = p1.x+u_a*(p2.x-p1.x)
	new_y = p1.y+u_a*(p2.y-p1.y)
	# pdb.set_trace()
	if (new_x>p1.x and new_x<p2.x) or (new_x>p2.x and new_x<p1.x):
		return True 
	elif u_a<=0 or u_a>=1 or u_b<=0 or u_b>=1:
		return False
	else:
		return True

def check_intersect(p1, p2, points_list):
	for i in range(0, len(points_list)):
		q1 = points_list[i]
		if q1 == p1:
			continue
		else:
			for q2 in points_list[i].edges:
				if intersect(p1,p2,q1,q2):
					print "True"
					return True
	print "False"
	return False

def form_edges(points_list, n):
	#dist_dict_list is a list of the other points, ordered by distance 
	dist_dict_list = []
	for i in range(0, n-1):
		new_dist_dict = {}
		for j in range(i+1, n):
			dist = calculate_distance(points_list[i].x, points_list[i].y, points_list[j].x, points_list[j].y)
			new_dist_dict.update({points_list[j]: dist})
		# print new_dist_dict
		new_dist_dict = sorted(new_dist_dict.items(), key = operator.itemgetter(1))
		# print new_dist_dict
		while new_dist_dict:
			adj_struct = new_dist_dict.pop(0)
			points_list[i].edge_candidate.append(adj_struct[0])
	print points_list
	# pdb.set_trace()
	total_edge = 0
	for j in range(0, n):
		for i in range(0, n-1):
			start_point = points_list[i]
			if not points_list[i].edge_candidate:
				continue
			end_point = points_list[i].edge_candidate.pop(0)
			if check_intersect(start_point, end_point, points_list):
				print "True"
				continue
			else:
				total_edge+=1
				points_list[i].edges.append(end_point)
	print total_edge
	for each_point in points_list:
		for each_edge in each_point.edges:
			plt.plot([each_point.x, each_edge.x], [each_point.y, each_edge.y])
			# plt.show()


def graph(n):
	graph_size = 50
	points_list = []
	exist_list = []
	for i in range(0,n):
		x = randint(0,graph_size)
		y = randint(0,graph_size)
		point_cord = (x, y)
		if point_cord in exist_list:
			while each_point in exist_list:
				x = randint(0,graph_size)
				y = randint(0,graph_size)
				point_cord = (x, y)
		new_point = Point(x, y)
		points_list.append(new_point)
		exist_list.append(point_cord)

	for each_point in points_list:
		plt.plot(each_point.x, each_point.y, 'ro')
	form_edges(points_list, n)

	plt.show()
	return points_list

# generate_graph(5)